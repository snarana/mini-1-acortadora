import webapp
import shelve
import random
import string
from urllib.parse import unquote

form = '<form action="" method="POST" class="form-example" ' \
       + '<div class="form-example">' \
       + '<label for="url">url: </label>' \
       + '<input type="text" name="url" id="url" required>' \
       + '</div>' \
       + '<div class="form-example">' \
       + '<input type="submit" value="Acortar!">' \
       + '</div></form>'

urls = {}
saved = shelve.open('saved')
urlnom = shelve.open('urlnom')


class randomshort(webapp.webApp):
    def parse(self, received):
        metodo = received.decode().split(' ')[0]
        recurso = received.decode().split(' ')[1]
        body = received.decode().split('\r\n\r\n')[1]
        body = unquote(body)
        print(body)
        return metodo, recurso, body

    def process(self, arg):
        metodo, recurso, body = arg
        if metodo == "GET":
            for key, value in saved.items():
                urls[key] = value
            if recurso == '/':
                http = "200 OK"
                html = "<html><body><h1>Introduce url para acortar " + form + \
                       "Las url's que has acortado son: " + str(urls) + "</h1></body></html>"
                return http, html
            else:
                if recurso in saved:
                    http = "301 Moved Permanently"
                    html = "<html><body><h1>HTTP REDIRECT: " + str(saved[recurso]) + "</h1></body></html>"
                    return http, html
                else:
                    http = "404 NOT FOUND"
                    html = "<html><body><h1>HTTP ERROR: Recurso no disponible</h1></body></html>"
                    return http, html

        if metodo == "POST":
            if "url" in body:
                short = body[4:]
                short = unquote(short)
                if short.startswith(('http://', 'https://')):
                    short = short
                else:
                    prefix = "https://"
                    short = prefix + short

                if short in urlnom:
                    pass
                else:
                    rec_aleat = ''.join(random.SystemRandom().choices(string.ascii_lowercase + string.digits, k=6))
                    if rec_aleat in urlnom.values():
                        return rec_aleat
                    saved['/' + rec_aleat] = short
                    urlnom[short] = str(rec_aleat)
                for key, value in saved.items():
                    urls[key] = value

                nexturl = "http://localhost:1234/"
                http = "200 OK"
                html = "<html><body><h1>Url acortada: " + '<a href="' + nexturl + str(urlnom[short]) + '">' + \
                       nexturl + str(urlnom[short]) + "</a></h1><br><h2>La url original es: " + \
                       '<a href="' + str(short) + '">' + str(short) + "</a></h2><br><br>" + form + \
                       "<br><br>" + "Ya has acortado estas urls: " + str(urls) + "</h1></body></html>"
                return http, html

            else:
                http = "404 NOT FOUND"
                html = "<html><body><h1>ERROR 404 NOT FOUND</h1></body></html>"
                return http, html


if __name__ == "__main__":
    try:
        randomshort = randomshort('localhost', 1233)
    except KeyboardInterrupt:
        saved.close()
        urlnom.close()
